#!/usr/bin/env python
# title           :hourly_price_profile_per_company_DIESEL.py
# description     :This will answer the question "What is a hourly price
#                  profile per company, gasoline type DIESEL"
# author          :Oliver Griebel
# date            :20160521
# version         :1.0
# usage           :python hourly_price_profile_per_company_DIESEL.py
# notes           :needs example_sprit_cut_prices.csv and
#                  example_sprit_cut_station.csv in the local folder
# python_version  :2.7.11
# ==============================================================================

# import the modules needed to run the script
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# load all data
orig_data_station = pd.read_csv('./example_sprit_cut_station.csv', parse_dates=['ID'], delimiter=';')
orig_data_prices = pd.read_csv('./example_sprit_cut_prices.csv', nrows=10000, parse_dates=['STID'], delimiter=';')

# extract necessary data
company = pd.DataFrame(orig_data_station, columns=['ID', 'BRAND'])
company = company.set_index('ID')

price = pd.DataFrame(orig_data_prices, columns=['STID', 'DATE_CHANGED', 'DIESEL'])

# join tables together
data = price.join(company, on='STID')

# prepare data frame for output
data = data.drop('STID', 1)
data['DATE_CHANGED'] = pd.DatetimeIndex(data['DATE_CHANGED'])
data = data.set_index('DATE_CHANGED')

# remove faulty entries
data = data[data['DIESEL'] >= 800]
data = data[data['DIESEL'] <= 2500]

# sort data
data_min = data.groupby(['BRAND', data.index.hour]).min()
data_max = data.groupby(['BRAND', data.index.hour]).max()

# create figure
fig = plt.figure('Hourly price profile for DIESEL by company')
fig.text(0.5, 0.04, 'Hour', ha='center')
fig.text(0.04, 0.5, 'Price', va='center', rotation='vertical')

# print result
print "What is a hourly price profile:"
print "   Per company, gasoline type"
print "      minimal price"
print data_min
print
print "      maximal price"
print data_max

#  brand name extraction
levels = data_min.index.levels[0]

# plot sub graphs of first 5 results
for x in np.arange(5):
    plt.subplot(511 + x)
    plt.plot(data_max.xs(levels[x], level=0), label='maximal price')
    plt.plot(data_min.xs(levels[x], level=0), label='minimal price')
    plt.text(1, 1800, levels[x])
    plt.axis([0, 23, 1000, 2000])
    plt.legend()

plt.show()
