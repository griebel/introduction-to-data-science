#!/usr/bin/env python
# title           :number_of_brands.py
# description     :This will answer the question "How many different brands
#                  are there"
# author          :Oliver Griebel
# date            :20160521
# version         :1.0
# usage           :python number_of_brands.py
# notes           :needs example_sprit_cut_station.csv in the local folder
# python_version  :2.7.11
# ==============================================================================

# import the modules needed to run the script
import pandas as pd

# load all data
orig_data_station = pd.read_csv('./example_sprit_cut_station.csv', parse_dates=['ID'], delimiter=';')

# search for amount of brands
brands = orig_data_station['BRAND'].value_counts()
number_of_brands = len(brands)

print "Q: How many different brands are there?"
print "A:", number_of_brands
