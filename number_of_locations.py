#!/usr/bin/env python
# title           :number_of_locations.py
# description     :This will answer the question "How many different locations
#                  are present in the data"
# author          :Oliver Griebel
# date            :20160521
# version         :1.0
# usage           :python number_of_locations.py
# notes           :needs example_sprit_cut_station.csv in the local folder
# python_version  :2.7.11
# ==============================================================================

# import the modules needed to run the script
import pandas as pd

# load all data
orig_data_station = pd.read_csv('./example_sprit_cut_station.csv', parse_dates=['ID'], delimiter=';')

# search for amount of stations
locations = orig_data_station['ID'].value_counts()
number_of_locations = len(locations)

print "Q: How many different locations are present in the data?"
print "A:", number_of_locations
