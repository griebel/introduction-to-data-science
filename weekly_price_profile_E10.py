#!/usr/bin/env python
# title           :weekly_price_profile_E10.py
# description     :This will answer the question "What is a weekly price
#                  profile of all companies for E10"
# author          :Oliver Griebel
# date            :20160521
# version         :1.0
# usage           :python weekly_price_profile_E10.py
# notes           :needs example_sprit_cut_prices.csv in the local folder
# python_version  :2.7.11
# ==============================================================================

# import the modules needed to run the script
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# load all data
orig_data_prices = pd.read_csv('./example_sprit_cut_prices.csv', nrows=10000, parse_dates=['STID'], delimiter=';')

# extract necessary data
price = pd.DataFrame(orig_data_prices, columns=['DATE_CHANGED', 'E10'])
price['DATE_CHANGED'] = pd.DatetimeIndex(price['DATE_CHANGED'])
price = price.set_index('DATE_CHANGED')
price = price.sort_index()

# remove faulty entries
price = price[price >= 800]
price = price[price <= 2500]

# sort data
price_min = price.groupby(price.index.week).min()
price_max = price.groupby(price.index.week).max()

# create index
index = np.arange(52)

# draw graph
plt.plot(price_max, label='maximal price')
plt.plot(price_min, label='minimal price')
plt.axis([1, 52, 1000, 2000])
plt.xlabel('Week of year')
plt.ylabel('Price')
plt.legend()
plt.title('Weekly price profile for E10')
plt.tight_layout()
plt.show()
