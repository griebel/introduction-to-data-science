#!/usr/bin/env python
# title           :min_max_prices_per_quarter_E5.py
# description     :This will answer the question "What is the min, max price
#                  for E5 per quarter"
# author          :Oliver Griebel
# date            :20160521
# version         :1.0
# usage           :python min_max_prices_per_quarter_E5.py
# notes           :needs example_sprit_cut_prices.csv in the local folder
# python_version  :2.7.11
# ==============================================================================

# import the modules needed to run the script
import pandas as pd

# load all data
orig_data_prices = pd.read_csv('./example_sprit_cut_prices.csv', parse_dates=['STID'], delimiter=';')

# extract necessary data
price = pd.DataFrame(orig_data_prices, columns=['DATE_CHANGED', 'E5'])
price['DATE_CHANGED'] = pd.to_datetime(price['DATE_CHANGED'])
price = price.set_index('DATE_CHANGED')
price = price.sort_index()

# remove faulty entries
price = price[price >= 800]
price = price[price <= 2500]

# sort data
price_min = price.resample('Q').min()
price_max = price.resample('Q').max()

print "Q: What is the min, max price for E5 per month?"
print "A: minimum"
print price_min
print
print "   maximum"
print price_max
