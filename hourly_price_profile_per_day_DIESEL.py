#!/usr/bin/env python
# title           :hourly_price_profile_per_day_DIESEL.py
# description     :This will answer the question "What is a weekly price
#                  profile of all companies for DIESEL"
# author          :Oliver Griebel
# date            :20160521
# version         :1.0
# usage           :python hourly_price_profile_per_day_DIESEL.py
# notes           :needs example_sprit_cut_prices.csv in the local folder
# python_version  :2.7.11
# ==============================================================================

# import the modules needed to run the script
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# load all data
orig_data_prices = pd.read_csv('./example_sprit_cut_prices.csv', nrows=10000, parse_dates=['STID'], delimiter=';')

# extract necessary data
price = pd.DataFrame(orig_data_prices, columns=['DATE_CHANGED', 'DIESEL'])
price['DATE_CHANGED'] = pd.DatetimeIndex(price['DATE_CHANGED'])
price = price.set_index('DATE_CHANGED')
price = price.sort_index()

# remove faulty entries
price = price[price >= 800]
price = price[price <= 2500]

# sort data
price_min = price.groupby([price.index.dayofweek, price.index.hour]).min()
price_max = price.groupby([price.index.dayofweek, price.index.hour]).max()

# days of week
days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

# create figure
fig = plt.figure('Hourly price profile for DIESEL by days')
fig.text(0.5, 0.04, 'Hour', ha='center')
fig.text(0.04, 0.5, 'Price', va='center', rotation='vertical')

# plot sub graphs
for x in np.arange(7):
    plt.subplot(711 + x)
    plt.plot(price_max.xs([x], level=0), label='maximal price')
    plt.plot(price_min.xs([x], level=0), label='minimal price')
    plt.text(1, 1800, days[x])
    plt.axis([0, 23, 1000, 2000])
    plt.legend()

plt.show()
